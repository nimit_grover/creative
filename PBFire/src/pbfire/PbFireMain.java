/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire;

import campaigns.BYUCampaign;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author root
 */
public class PbFireMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        //FileUtils.copyURLToFile(new URL(log4j2), new File(log4jConfigPath));

        // read all files and load in objects
        // fire the pixel
        ObjectFiller.fillCountry();
        ObjectFiller.imeiFiller();
        ObjectFiller.macFiller();
        ObjectFiller.fillHardware();

        BYUCampaign bYUCampaign = new BYUCampaign();
        bYUCampaign.start(4);
    }
}
