/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import pbfire.objects.Carrier;
import pbfire.objects.CountryObj;
import pbfire.objects.HardwareObj;
import pbfire.objects.ImeiObj;
import pbfire.objects.MacAddressObj;

/**
 *
 * @author root
 */
public class ObjectFiller {

    public static List<CountryObj> fillCountry() throws IOException {
        List<CountryObj> list = new ArrayList<CountryObj>();
        List<Carrier> countryCarriers = new ArrayList<Carrier>();

        try ( CSVReader reader = new CSVReader(new FileReader("C:\\Users\\panka\\Documents\\NetBeansProjects\\PBFire\\data\\country_data.csv"))) {
            List<String[]> r = reader.readAll();
            r.remove(0);

            r.forEach((String[] x) -> {
                System.out.println(Arrays.toString(x));

//                String[] tempArray = x[2].split("\\}$\\{");
                String[] tempArray;
                tempArray = x[2].split(",");

//                for(String abc:tempArray){
//                    System.out.print(abc+" ");
//                }
//
//                 IntStream.range(0, tempArray.length).forEach(i ->
//        // replace non-empty sequences
//        // of whitespace characters
//        tempArray[i] = tempArray[i].replaceAll("$", " "));
//
//                System.out.println(tempArray.length);
                for (int i = 0; i < tempArray.length; i++) {
                    tempArray[i] = tempArray[i].replaceAll(",", " ");
                }

                countryCarriers.add(new Carrier(tempArray[0], Integer.parseInt(tempArray[1])));

                //replaceAll("\\]", "")
                // /       .replaceAll("\\\\", "")
                //        .replaceAll("\\),\\(", ";")
//                //       .replaceAll("\\(", "")
//                //      .replaceAll("\\)", "");
//                String[] ccArray = x[2].split(",");
//                Gson gson = new Gson();
//                //gson.fromJson(jsonStr, Carrier.class);
//                for (int i = 0; i < ccArray.length; i++) {
//                    countryCarriers.add(new Carrier(ccArray[0],
//                            Integer.parseInt(ccArray[1])));
//                }
                list.add(new CountryObj(x[0], Integer.parseInt(x[1]), countryCarriers, x[3], x[4]));
            });
        }
        // print details of Bean object
        return list;
    }

    public static List<ImeiObj> imeiFiller() throws IOException {
        List<ImeiObj> list = new ArrayList<ImeiObj>();

        try ( CSVReader reader = new CSVReader(new FileReader("C:\\Users\\panka\\Documents\\NetBeansProjects\\PBFire\\data\\imei_map.csv"))) {
            List<String[]> r = reader.readAll();

            r.forEach((String[] x) -> {
                System.out.println(Arrays.toString(x));
                System.out.println(x.length);
                List<Integer> imeiObjsList = new ArrayList<Integer>();
                for (int i = 1; i < x.length; i++) {
                    imeiObjsList.add(Integer.parseInt(x[i]));
                }
                list.add(new ImeiObj(x[0], imeiObjsList));

            });
        }
        return list;
    }

    public static List<MacAddressObj> macFiller() throws IOException {
        List<MacAddressObj> list = new ArrayList<MacAddressObj>();

        try ( CSVReader reader = new CSVReader(new FileReader("C:\\Users\\panka\\Documents\\NetBeansProjects\\PBFire\\data\\mac_address_map.csv"))) {
            List<String[]> r = reader.readAll();

            r.forEach((String[] x) -> {
                System.out.println(Arrays.toString(x));

                List<String> macObjsList = new ArrayList<String>();

                for (int i = 1; i < x.length; i++) {
                    macObjsList.add(x[i]);
                }

                list.add(new MacAddressObj(x[0], macObjsList));

            });
        }
        return list;
    }

    public static List<HardwareObj> fillHardware() throws IOException {
        List<HardwareObj> list = new ArrayList<HardwareObj>();

        try ( CSVReader reader = new CSVReader(new FileReader("C:\\Users\\panka\\Documents\\NetBeansProjects\\PBFire\\data\\hardware_data.csv"))) {
            List<String[]> r = reader.readAll();
            r.remove(0);
            String[] hardwareObjPass = new String[32];
            String space = "  ";

            r.forEach((String[] x) -> {
                System.out.println(Arrays.toString(x));

                if (x[4].equals(space)) {
                    for (int i = 0; i < 32; i++) {
                        if (i < 4) {
                            hardwareObjPass[i] = x[i];
                        }
                        if (i == 4) {
                            hardwareObjPass[i] = "0";
                        }
                        if (i > 4) {
                            hardwareObjPass[i] = x[i];
                        }
                    }

                }

                if (space.equals(x[16])) {
                    for (int j = 0; j < 32; j++) {
                        if (j < 16) {
                            hardwareObjPass[j] = x[j];
                        }
                        if (j == 16) {
                            hardwareObjPass[j] = "0";
                        }
                        if (j > 16) {
                            hardwareObjPass[j] = x[j];
                        }
                    }

                }

                if (space.equals(x[17])) {
                    for (int k = 0; k < 32; k++) {
                        if (k < 17) {
                            hardwareObjPass[k] = x[k];
                        }
                        if (k == 17) {
                            hardwareObjPass[k] = "0";
                        }
                        if (k > 17) {
                            hardwareObjPass[k] = x[k];
                        }
                    }

                }
                if (space.equals(x[24])) {
                    for (int l = 0; l < 32; l++) {
                        if (l < 24) {
                            hardwareObjPass[l] = x[l];
                        }
                        if (l == 24) {
                            hardwareObjPass[l] = "0";
                        }
                        if (l > 24) {
                            hardwareObjPass[l] = x[l];
                        }
                    }

                }

                if (space.equals(x[25])) {
                    for (int m = 0; m < 32; m++) {
                        if (m < 25) {
                            hardwareObjPass[m] = x[m];
                        }
                        if (m == 25) {
                            hardwareObjPass[m] = "0";
                        }
                        if (m > 25) {
                            hardwareObjPass[m] = x[m];
                        }
                    }

                }
//                  for(int i=0; i<x.length;i++){  
//                y[i]=y[i].replaceAll(",", " ");}
//                  System.out.println(x.length);
                list.add(new HardwareObj(hardwareObjPass[0],
                        hardwareObjPass[1],
                        hardwareObjPass[2],
                        hardwareObjPass[3],
                        Integer.parseInt(hardwareObjPass[4]),
                        hardwareObjPass[5],
                        hardwareObjPass[6],
                        hardwareObjPass[7],
                        hardwareObjPass[8],
                        hardwareObjPass[9],
                        hardwareObjPass[10],
                        hardwareObjPass[11],
                        hardwareObjPass[12],
                        hardwareObjPass[13],
                        hardwareObjPass[14],
                        hardwareObjPass[15],
                        Integer.parseInt(hardwareObjPass[16]),
                        Integer.parseInt(hardwareObjPass[17]),
                        hardwareObjPass[18],
                        hardwareObjPass[19],
                        hardwareObjPass[20],
                        hardwareObjPass[21],
                        hardwareObjPass[22],
                        hardwareObjPass[23],
                        Float.parseFloat(hardwareObjPass[24]),
                        Float.parseFloat(hardwareObjPass[25]),
                        hardwareObjPass[26],
                        hardwareObjPass[27],
                        hardwareObjPass[28],
                        hardwareObjPass[29],
                        hardwareObjPass[30],
                        hardwareObjPass[31]));
            });

        }
        return list;
    }

}
