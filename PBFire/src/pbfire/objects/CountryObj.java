/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire.objects;

import java.util.List;

/**
 *
 * @author root
 */
public class CountryObj {

    private String country;
    private Integer mcc;
    private String carrierString;
    List<Carrier> carrier;
    private String locale;
    private String timezone;

    public String getCarrierString() {
        return carrierString;
    }

    public void setCarrierString(String carrierString) {
        this.carrierString = carrierString;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getMcc() {
        return mcc;
    }

    public void setMcc(Integer mcc) {
        this.mcc = mcc;
    }

    public List<Carrier> getCarrier() {
        return carrier;
    }

    public void setCarrier(List<Carrier> carrier) {
        this.carrier = carrier;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public CountryObj(String country, Integer mcc, List<Carrier> carrier, String locale, String timezone) {
        this.country = country;
        this.mcc = mcc;
        this.carrier = carrier;
        this.locale = locale;
        this.timezone = timezone;
    }


}
