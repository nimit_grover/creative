/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire.objects;

import java.util.List;

/**
 *
 * @author root
 */
public class ImeiObj {
    private String brandName;
    private List<Integer> imeiStart;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public List<Integer> getImeiStart() {
        return imeiStart;
    }

    public void setImeiStart(List<Integer> imeiStart) {
        this.imeiStart = imeiStart;
    }

    public ImeiObj(String brandName, List<Integer> imeiStart) {
        this.brandName = brandName;
        this.imeiStart = imeiStart;
    }
}
