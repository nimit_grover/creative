/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire.objects;

/**
 *
 * @author root
 */
public class Carrier {
    private String carrierName;
    private Integer carrierNumber;

    public Carrier(String carrierName, Integer carrierNumber) {
        this.carrierName = carrierName;
        this.carrierNumber = carrierNumber;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public Integer getCarrierNumber() {
        return carrierNumber;
    }

    public void setCarrierNumber(Integer carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

}
