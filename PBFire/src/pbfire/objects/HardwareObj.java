/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire.objects;

/**
 *
 * @author root
 */
public class HardwareObj {
    private String model;
    private String manufacturer;
    private String cpu_abi;
    private String cpu_abi2;
    private Integer dpi;
    private String device;
    private String os_details_id;
    private String fingerprint;
    private String brand;
    private String ro_build_product;
    private String mainboard;
    private String resolution;
    private String tags;
    private String os_codename;
    private String android_version_I_part;
    private String android_version_II_part;
    private Integer sdk;
    private Integer cpu_core;
    private String evaluated_size;
    private String gpu_part_I;
    private String gpu_part_II;
    private String gpu_part_III;
    private String ram;
    private String ram_available;
    private Float flash;
    private Float flash_actual;
    private String kernel;
    private String bluetooth;
    private String processor;
    private String hardware;
    private String os_incremental;
    private String name;

    public HardwareObj(String model, String manufacturer, String cpu_abi, String cpu_abi2, Integer dpi, String device, String os_details_id, String fingerprint, String brand, String ro_build_product, String mainboard, String resolution, String tags, String os_codename, String android_version_I_part, String android_version_II_part, Integer sdk, Integer cpu_core, String evaluated_size, String gpu_part_I, String gpu_part_II, String gpu_part_III, String ram, String ram_available, Float flash, Float flash_actual, String kernel, String bluetooth, String processor, String hardware, String os_incremental, String name) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.cpu_abi = cpu_abi;
        this.cpu_abi2 = cpu_abi2;
        this.dpi = dpi;
        this.device = device;
        this.os_details_id = os_details_id;
        this.fingerprint = fingerprint;
        this.brand = brand;
        this.ro_build_product = ro_build_product;
        this.mainboard = mainboard;
        this.resolution = resolution;
        this.tags = tags;
        this.os_codename = os_codename;
        this.android_version_I_part = android_version_I_part;
        this.android_version_II_part = android_version_II_part;
        this.sdk = sdk;
        this.cpu_core = cpu_core;
        this.evaluated_size = evaluated_size;
        this.gpu_part_I = gpu_part_I;
        this.gpu_part_II = gpu_part_II;
        this.gpu_part_III = gpu_part_III;
        this.ram = ram;
        this.ram_available = ram_available;
        this.flash = flash;
        this.flash_actual = flash_actual;
        this.kernel = kernel;
        this.bluetooth = bluetooth;
        this.processor = processor;
        this.hardware = hardware;
        this.os_incremental = os_incremental;
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCpu_abi() {
        return cpu_abi;
    }

    public void setCpu_abi(String cpu_abi) {
        this.cpu_abi = cpu_abi;
    }

    public String getCpu_abi2() {
        return cpu_abi2;
    }

    public void setCpu_abi2(String cpu_abi2) {
        this.cpu_abi2 = cpu_abi2;
    }

    public Integer getDpi() {
        return dpi;
    }

    public void setDpi(Integer dpi) {
        this.dpi = dpi;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getOs_details_id() {
        return os_details_id;
    }

    public void setOs_details_id(String os_details_id) {
        this.os_details_id = os_details_id;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRo_build_product() {
        return ro_build_product;
    }

    public void setRo_build_product(String ro_build_product) {
        this.ro_build_product = ro_build_product;
    }

    public String getMainboard() {
        return mainboard;
    }

    public void setMainboard(String mainboard) {
        this.mainboard = mainboard;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getOs_codename() {
        return os_codename;
    }

    public void setOs_codename(String os_codename) {
        this.os_codename = os_codename;
    }

    public String getAndroid_version_I_part() {
        return android_version_I_part;
    }

    public void setAndroid_version_I_part(String android_version_I_part) {
        this.android_version_I_part = android_version_I_part;
    }

    public String getAndroid_version_II_part() {
        return android_version_II_part;
    }

    public void setAndroid_version_II_part(String android_version_II_part) {
        this.android_version_II_part = android_version_II_part;
    }

    public Integer getSdk() {
        return sdk;
    }

    public void setSdk(Integer sdk) {
        this.sdk = sdk;
    }

    public Integer getCpu_core() {
        return cpu_core;
    }

    public void setCpu_core(Integer cpu_core) {
        this.cpu_core = cpu_core;
    }

    public String getEvaluated_size() {
        return evaluated_size;
    }

    public void setEvaluated_size(String evaluated_size) {
        this.evaluated_size = evaluated_size;
    }

    public String getGpu_part_I() {
        return gpu_part_I;
    }

    public void setGpu_part_I(String gpu_part_I) {
        this.gpu_part_I = gpu_part_I;
    }

    public String getGpu_part_II() {
        return gpu_part_II;
    }

    public void setGpu_part_II(String gpu_part_II) {
        this.gpu_part_II = gpu_part_II;
    }

    public String getGpu_part_III() {
        return gpu_part_III;
    }

    public void setGpu_part_III(String gpu_part_III) {
        this.gpu_part_III = gpu_part_III;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getRam_available() {
        return ram_available;
    }

    public void setRam_available(String ram_available) {
        this.ram_available = ram_available;
    }

    public Float getFlash() {
        return flash;
    }

    public void setFlash(Float flash) {
        this.flash = flash;
    }

    public Float getFlash_actual() {
        return flash_actual;
    }

    public void setFlash_actual(Float flash_actual) {
        this.flash_actual = flash_actual;
    }

    public String getKernel() {
        return kernel;
    }

    public void setKernel(String kernel) {
        this.kernel = kernel;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(String bluetooth) {
        this.bluetooth = bluetooth;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getOs_incremental() {
        return os_incremental;
    }

    public void setOs_incremental(String os_incremental) {
        this.os_incremental = os_incremental;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
