/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire.objects;

import java.util.List;

/**
 *
 * @author root
 */
public class MacAddressObj {
     private List<String> macAddressStart;
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getMacAddressStart() {
        return macAddressStart;
    }

    public void setMacAddressStart(List<String> macAddressStart) {
        this.macAddressStart = macAddressStart;
    }

    public MacAddressObj(String brand, List<String> macAddressStart) {
        this.brand = brand;
        this.macAddressStart = macAddressStart;
    }
   

}
