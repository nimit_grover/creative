/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire;

import pbfire.objects.CampaignObj;
import pbfire.log.LoggerFactory;
import pbfire.log.MyLogger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author root
 */
public class ReadJson {

    private MyLogger LOGGER = LoggerFactory.getLogger(PbFireMain.class);

    public static CampaignObj jsonToObj(String url) {
        CampaignObj campaignObj = null;
        URLConnection conn1 = null;
        try {
            conn1 = new URL(url).openConnection();
            conn1.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            BufferedReader intt = new BufferedReader(new InputStreamReader(conn1.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine1;
            while ((inputLine1 = intt.readLine()) != null) {
                System.out.println("get data :" + inputLine1);
                response.append(inputLine1);
            }
            intt.close();
            System.out.println("get data :" + response);
            System.out.println("Contents of the JSON are: ");
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(response.toString());

            campaignObj = (CampaignObj) jsonObject.get("NUMBER_OF_INSTANCES");
        } catch (IOException | ParseException e) {

        }
        return campaignObj;
    }
}
