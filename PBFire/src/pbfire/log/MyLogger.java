/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbfire.log;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

/**
 * Logger class create logs it will print the logs on console with
 */
public class MyLogger {

    private final boolean mLoggable;
    private final Logger mLogger;
    private final String filePath;

    public MyLogger(String mClassName) {

        LoggerContext context = (LoggerContext) LogManager.getContext(false);
        File file = new File("/projects/PBFire/conf/log4j2.xml");

        // this will force a reconfiguration
        context.setConfigLocation(file.toURI());
        this.mLoggable = true;
        this.mLogger = LogManager.getLogger(mClassName);
        //this.ThreadNo = Logger.getLogger(ThreadNo);
        DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyy");
        Date date = new Date();

        filePath = System.getProperty("user.home") + "/" + "log_report_" + dateFormat.format(date) + ".txt";
        //setLoggerHander(filePath);
        //removeConsoleLogger();
    }

    /**
     * To print the formatted info message
     *
     * @param format String format
     * @param arguments Arguments
     */
    public void info(String format, Object... arguments) {
        if (mLoggable) {
            mLogger.log(Level.INFO, String.format(format, arguments));
        }
    }

    /**
     * To print the formatted error
     *
     * @param format String format
     * @param arguments Arguments
     */
    public void error(String format, Object... arguments) {
        if (mLoggable) {
            mLogger.log(Level.ERROR, String.format(format, arguments));
        }
    }

    /**
     * Set logger handler will log into a file
     *
     * @param fileName file path where want to access log
     */
//    public void setLoggerHander(String fileName) {
//        try {
//            FileHandler fh = new FileHandler(fileName, true);
//            mLogger.addHandler(fh);
//            SimpleFormatter formatter = new SimpleFormatter();
//            fh.setFormatter(formatter);
//        } catch (IOException ex) {
//            Logger.getLogger(MyLogger.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SecurityException ex) {
//            Logger.getLogger(MyLogger.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    /**
     * Get the logging file full path
     *
     * @return file path
     */
    public String getFileName() {
        return filePath;
    }

    public File getFile() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
