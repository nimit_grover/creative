/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campaigns;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 *
 * @author root
 */
public class BYUCampaign {

    public void start(Integer noOfConversions) throws MalformedURLException, IOException {
        //String url1 = "";
        //CampaignObj testCampaignObj = ReadJson.jsonToObj(url1);
        //testCampaignObj.toString();
        Integer max = 100;
        Integer min = 50;
        Integer j = (int) (Math.random() * ((max - min) + 1)) + min;

        for (int k = 0; k < noOfConversions; k++) {
            for (int i = 0; i < j; i++) {
                sendClick();
            }
            sendConversion();
        }
    }

    public void sendClick() throws MalformedURLException, IOException {
        // click
        //http://user-test:Test@123#@us.smartproxy.com:10000
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("http://user-test-sessionduration-1:Test@123#@us.smartproxy.com", 10001));
        URL url1 = new URL("https://app.adjust.net.in/session");
        URLConnection con1 = url1.openConnection(proxy);
        HttpURLConnection http1 = (HttpURLConnection) con1;
        http1.setRequestMethod("GET"); // PUT is another valid option
        http1.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http1.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http1.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http1.setRequestProperty("Host", "app.adjust.net.in");
        http1.setRequestProperty("Connection", "Keep-Alive");
        http1.setRequestProperty("Accept-Encoding", "gzip");
        http1.setRequestProperty("Content-Length", "856");
        http1.setDoOutput(true);
    }

    public static void sendConversion() throws MalformedURLException, IOException {
        //send click
        // session request
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("http://user-test-sessionduration-1:Test@123#@us.smartproxy.com", 10001));
        URL url1 = new URL("https://app.adjust.net.in/session");
        URLConnection con1 = url1.openConnection(proxy);
        HttpURLConnection http1 = (HttpURLConnection) con1;
        http1.setRequestMethod("POST"); // PUT is another valid option
        http1.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http1.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http1.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http1.setRequestProperty("Host", "app.adjust.net.in");
        http1.setRequestProperty("Connection", "Keep-Alive");
        http1.setRequestProperty("Accept-Encoding", "gzip");
        http1.setRequestProperty("Content-Length", "856");
        http1.setDoOutput(true);

        String rawData1 = "updated_at=2021-09-26T09%3A33%3A46.533Z%2B0530"
                + "&device_type=phone&device_manufacturer=samsung&"
                + "gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea&environment=production"
                + "&session_count=1&attribution_deeplink=1&"
                + "android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c"
                + "&connectivity_type=1&os_build=MMB29Q&needs_response_details=1"
                + "&api_level=23&event_buffering_enabled=0&os_name=android"
                + "&display_width=720&screen_format=long&screen_density=high"
                + "&hardware_name=MMB29Q.J210FXXU0ARE6&tracking_enabled=1"
                + "&gps_adid_src=service&package_name=com.byu.id"
                + "&installed_at=2021-09-26T09%3A33%3A46.533Z%2B0530"
                + "&app_version=706&network_type=3&country=GB&gps_adid_attempt=1"
                + "&screen_size=normal&app_token=kq5cfsvxuwow&device_name=SM-J210F"
                + "&display_height=1280&created_at=2021-09-26T09%3A34%3A14.285Z%2B0530"
                + "&os_version=6.0.1&language=en&cpu_type=armeabi-v7a"
                + "&sent_at=2021-09-26T09%3A34%3A14.506Z%2B0530";

        String encodedData1 = URLEncoder.encode(rawData1, "UTF-8");
        OutputStream os1 = con1.getOutputStream();
        os1.write(encodedData1.getBytes());

        // second call
        URL url2 = new URL("https://app.adjust.net.in/sdk_click");
        URLConnection con2 = url2.openConnection(proxy);
        HttpURLConnection http2 = (HttpURLConnection) con2;
        http2.setRequestMethod("POST"); // PUT is another valid option
        http2.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http2.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http2.setRequestProperty("Host", "app.adjust.net.in");
        http2.setRequestProperty("Connection", "Keep-Alive");
        http2.setRequestProperty("Accept-Encoding", "gzip");
        http2.setRequestProperty("Content-Length", "856");
        http2.setDoOutput(true);

        String rawData2 = "updated_at=2021-09-26T09%3A33%3A46.533Z%2B0530"
                + "&google_play_instant=0&device_type=phone&source=install_referrer"
                + "&referrer=utm_source%3Dgoogle-play%26utm_medium%3Dorganic"
                + "&device_manufacturer=samsung&"
                + "gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea&environment=production"
                + "&session_count=1&time_spent=0&referrer_api=google"
                + "&attribution_deeplink=1"
                + "&android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c"
                + "&connectivity_type=1&os_build=MMB29Q&needs_response_details=1"
                + "&api_level=23&event_buffering_enabled=0&os_name=android"
                + "&display_width=720&subsession_count=1&screen_format=long"
                + "&screen_density=high&hardware_name=MMB29Q.J210FXXU0ARE6"
                + "&tracking_enabled=1&gps_adid_src=service&session_length=0"
                + "&package_name=com.byu.id"
                + "&installed_at=2021-09-26T09%3A33%3A46.533Z%2B0530"
                + "&app_version=706&network_type=3&country=GB&gps_adid_attempt=1"
                + "&screen_size=normal&app_token=kq5cfsvxuwow&device_name=SM-J210F"
                + "&display_height=1280&created_at=2021-09-26T09%3A34%3A14.536Z%2B0530"
                + "&os_version=6.0.1&language=en&cpu_type=armeabi-v7a"
                + "&sent_at=2021-09-26T09%3A34%3A14.927Z%2B0530";

        String encodedData2 = URLEncoder.encode(rawData2, "UTF-8");
        OutputStream os2 = con2.getOutputStream();
        os2.write(encodedData2.getBytes());

        // call3
        URL url3 = new URL("https://app.adjust.net.in/event");
        URLConnection con3 = url3.openConnection(proxy);
        HttpURLConnection http3 = (HttpURLConnection) con3;
        http3.setRequestMethod("POST"); // PUT is another valid option
        http3.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http3.setRequestProperty("Authorization", "Signature "
                + "signature=\"EA5C6C6D8FC37EAD749E8AF94159B4F41B47C4AAF6FA19C45E73869E7F9E7AB1847CF49AA642E5BA4B40546934"
                + "E4DC0A94C1D92891B74F26FB17C4B8AC4F983446EE328DCEFD1DB41EDF8883705C8F6595A5100D78008679E9D88DBB15F3FA73\","
                + "secret_id=\"4\",algorithm=\"adj3\",headers_id=\"3\",native_version=\"2.6.1\"");
        http3.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http3.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http3.setRequestProperty("Host", "app.adjust.net.in");
        http3.setRequestProperty("Connection", "Keep-Alive");
        http3.setRequestProperty("Accept-Encoding", "gzip");
        http3.setRequestProperty("Content-Length", "921");
        http3.setDoOutput(true);

        String rawData3 = "device_type=phone&device_manufacturer=samsung&gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea"
                + "&environment=production&session_count=1&time_spent=3&attribution_deeplink=1"
                + "&android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c&connectivity_type=1&os_build=MMB29Q"
                + "&needs_response_details=1&api_level=23&event_buffering_enabled=0&os_name=android"
                + "&callback_params=%7B%22smartAttacker%22%3A%22openApplication%22%7D&display_width=720"
                + "&subsession_count=1&screen_format=long&screen_density=high&hardware_name=MMB29Q.J210FXXU0ARE6"
                + "&tracking_enabled=1&gps_adid_src=service&session_length=3&package_name=com.byu.id&event_count=1"
                + "&app_version=706&network_type=3&country=GB&gps_adid_attempt=1&screen_size=normal"
                + "&app_token=kq5cfsvxuwow&device_name=SM-J210F&display_height=1280"
                + "&created_at=2021-09-26T09%3A34%3A16.814Z%2B0530&os_version=6.0.1&language=en&event_token=ixseev"
                + "&cpu_type=armeabi-v7a&queue_size=1&sent_at=2021-09-26T09%3A36%3A08.251Z%2B0530";

        String encodedData3 = URLEncoder.encode(rawData3, "UTF-8");
        OutputStream os3 = con3.getOutputStream();
        os3.write(encodedData3.getBytes());

        // call 4
        URL url4 = new URL("https://app.adjust.net.in/sdk_info");
        URLConnection con4 = url4.openConnection(proxy);

        HttpURLConnection http4 = (HttpURLConnection) con4;
        http4.setRequestMethod("POST"); // PUT is another valid option
        http4.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http4.setRequestProperty("Authorization", "Signature signature=\"2C18C38FADC5AB63BBC137BC89D136587E1132F01543F95C61FE61F6AFAFC39CFC62BEA"
                + "35FF58CB36FA99269055DCDEE791272F268E6DA3AC7D9E3222F9F60233FACCF5BFA43ABBC87265038AAA08A629FC085170B87A2CF95F0F0BA5E0E78F6\","
                + "secret_id=\"4\",algorithm=\"adj3\",headers_id=\"3\",native_version=\"2.6.1\"");
        http4.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http4.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http4.setRequestProperty("Host", "app.adjust.net.in");
        http4.setRequestProperty("Connection", "Keep-Alive");
        http4.setRequestProperty("Accept-Encoding", "gzip");
        http4.setRequestProperty("Content-Length", "554");
        http4.setDoOutput(true);

        String rawData4 = "source=push&gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea"
                + "&environment=production&attribution_deeplink=1"
                + "&android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c&tracking_enabled=1"
                + "&gps_adid_src=service&needs_response_details=1&gps_adid_attempt=1"
                + "&app_token=kq5cfsvxuwow"
                + "&push_token=f_kyzU_vRW-r8rPKE2Dl7z%3AAPA91bHuQ87PLIxICV6-aJdsWoI"
                + "Pm8ywJ6OxJODlEYG3Stc9OZ-4vuSYYm6IZ_mRAYbTn_alu8AZ0Mq7qDrIHe-amVI"
                + "Imi6mvTIYGK7fqKpjz50FTRDCSKsRFbnWnM4qOOt9T1GRkV2V"
                + "&created_at=2021-09-26T09%3A34%3A17.452Z%2B0530"
                + "&event_buffering_enabled=0&sent_at=2021-09-26T09%3A36%3A09.259Z%2B0530";
        String encodedData4 = URLEncoder.encode(rawData4, "UTF-8");
        OutputStream os4 = con4.getOutputStream();
        os4.write(encodedData4.getBytes());

        // call 5
        URL url5 = new URL("https://app.adjust.net.in/attribution");
        URLConnection con5 = url5.openConnection(proxy);
        HttpURLConnection http5 = (HttpURLConnection) con5;
        http5.setRequestMethod("POST"); // PUT is another valid option
        http5.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http5.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http5.setRequestProperty("Host", "app.adjust.net.in");
        http5.setRequestProperty("Connection", "Keep-Alive");
        http5.setRequestProperty("Accept-Encoding", "gzip");
        http5.setDoOutput(true);

        String rawData5 = "device_type=phone"
                + "&gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea&environment=production"
                + "&attribution_deeplink=1"
                + "&android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c"
                + "&needs_response_details=1&api_level=23"
                + "&push_token=f_kyzU_vRW-r8rPKE2Dl7z%3AAPA91bHuQ87PLIxICV6-aJdsWoI"
                + "Pm8ywJ6OxJODlEYG3Stc9OZ-4vuSYYm6IZ_mRAYbTn_alu8AZ0Mq7qDrIHe-amVI"
                + "Imi6mvTIYGK7fqKpjz50FTRDCSKsRFbnWnM4qOOt9T1GRkV2V"
                + "&event_buffering_enabled=0&os_name=android&tracking_enabled=1"
                + "&gps_adid_src=service&package_name=com.byu.id&initiated_by=backend"
                + "&app_version=706&gps_adid_attempt=1&app_token=kq5cfsvxuwow"
                + "&device_name=SM-J210F&created_at=2021-09-26T09%3A36%3A10.290Z%2B"
                + "0530&os_version=6.0.1&sent_at=2021-09-26T09%3A36%3A10.352Z%2B0530";

        String encodedData5 = URLEncoder.encode(rawData5, "UTF-8");
        OutputStream os5 = con5.getOutputStream();
        os5.write(encodedData5.getBytes());

        // call 6
        URL url6 = new URL("https://app.adjust.net.in/attribution");
        URLConnection con6 = url6.openConnection(proxy);
        HttpURLConnection http6 = (HttpURLConnection) con6;
        http6.setRequestMethod("POST"); // PUT is another valid option
        http6.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http6.setRequestProperty("Authorization", "Signature signature=\"7A61867B112227BC04B5F829D9F14B379145A0A8B9B884B3606DFF3DBA58C51F03EB0A8E18983A7451D3C33702D3DC5EB89005D6BD829C7DCAEB2A42266133634D6231667FFE2D9959FA00EC819641CC1A1FEBEB112A87487EAC712D58038A59\",secret_id=\"4\",algorithm=\"adj3\",headers_id=\"3\",native_version=\"2.6.1\"");
        http6.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http6.setRequestProperty("Host", "app.adjust.net.in");
        http6.setRequestProperty("Connection", "Keep-Alive");
        http6.setRequestProperty("Accept-Encoding", "gzip");
        http6.setDoOutput(true);

        String rawData6 = "device_type=phone"
                + "&gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea&environment=production"
                + "&attribution_deeplink=1"
                + "&android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c&secret_id=4"
                + "&needs_response_details=1&api_level=23"
                + "&push_token=f_kyzU_vRW-r8rPKE2Dl7z%3AAPA91bHuQ87PLIxICV6-aJdsWoI"
                + "Pm8ywJ6OxJODlEYG3Stc9OZ-4vuSYYm6IZ_mRAYbTn_alu8AZ0Mq7qDrIHe-amVI"
                + "Imi6mvTIYGK7fqKpjz50FTRDCSKsRFbnWnM4qOOt9T1GRkV2V"
                + "&event_buffering_enabled=0&os_name=android"
                + "&signature=7A61867B112227BC04B5F829D9F14B379145A0A8B9B884B3606DF"
                + "F3DBA58C51F03EB0A8E18983A7451D3C33702D3DC5EB89005D6BD829C7DCAEB2"
                + "A42266133634D6231667FFE2D9959FA00EC819641CC1A1FEBEB112A87487EAC7"
                + "12D58038A59&native_version=2.6.1&tracking_enabled=1"
                + "&gps_adid_src=service&headers_id=3&package_name=com.byu.id"
                + "&initiated_by=sdk&app_version=706&gps_adid_attempt=1"
                + "&app_token=kq5cfsvxuwow&device_name=SM-J210F"
                + "&created_at=2021-09-26T09%3A36%3A43.739Z%2B0530&os_version=6.0.1"
                + "&algorithm=adj3&sent_at=2021-09-26T09%3A36%3A43.892Z%2B0530";

        String encodedData6 = URLEncoder.encode(rawData6, "UTF-8");
        OutputStream os6 = con6.getOutputStream();
        os6.write(encodedData6.getBytes());

        // call 7
        URL url7 = new URL("https://app.adjust.net.in/sdk_click");
        URLConnection con7 = url7.openConnection(proxy);
        HttpURLConnection http7 = (HttpURLConnection) con7;
        http7.setRequestMethod("POST"); // PUT is another valid option
        http7.setRequestProperty("Client-SDK", "react-native4.28.0@android4.27.0");
        http7.setRequestProperty("Authorization", "Signature signature=\"145F9B93B8EF9C1744D4C0A6959EF8858AC98F048E4B754DCF70531B141516EA9F6CEFBF25D9187ED13B0B53EBAFB237506360378C426A1D06C35C62943D8CD2FF3B7A546B5D7E859EF4F71ECB783EDE6EF0C04099FDEAB36E276FCEA1CDF8AE\",secret_id=\"4\",algorithm=\"adj3\",headers_id=\"3\",native_version=\"2.6.1");
        http7.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http7.setRequestProperty("User-Agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; SM-J210F Build/MMB29Q)");
        http7.setRequestProperty("Host", "app.adjust.net.in");
        http7.setRequestProperty("Connection", "Keep-Alive");
        http7.setRequestProperty("Accept-Encoding", "gzip");
        http7.setRequestProperty("Content-Length", "1089");
        http7.setDoOutput(true);

        String rawData7 = "device_type=phone&device_manufacturer=samsung"
                + "&gps_adid=e3519506-f8da-4cbf-8409-c71b99db29ea&environment=production"
                + "&session_count=1&time_spent=694&attribution_deeplink=1"
                + "&android_uuid=e3083a0e-a12c-419d-9c57-d832bd1a412c&connectivity_type=1"
                + "&os_build=MMB29Q&needs_response_details=1&api_level=23"
                + "&push_token=f_kyzU_vRW-r8rPKE2Dl7z%3AAPA91bHuQ87PLIxICV6-aJdsWoI"
                + "Pm8ywJ6OxJODlEYG3Stc9OZ-4vuSYYm6IZ_mRAYbTn_alu8AZ0Mq7qDrIHe-amVI"
                + "Imi6mvTIYGK7fqKpjz50FTRDCSKsRFbnWnM4qOOt9T1GRkV2V"
                + "&event_buffering_enabled=0&os_name=android"
                + "&callback_params=%7B%22smartAttacker%22%3A%22openApplication%22%7D"
                + "&display_width=720&subsession_count=8&screen_format=long"
                + "&screen_density=high&hardware_name=MMB29Q.J210FXXU0ARE6"
                + "&tracking_enabled=1&gps_adid_src=service&session_length=809"
                + "&package_name=com.byu.id&event_count=2&app_version=706"
                + "&network_type=3&country=GB&gps_adid_attempt=1&screen_size=normal"
                + "&app_token=kq5cfsvxuwow&device_name=SM-J210F&display_height=1280"
                + "&created_at=2021-09-26T09%3A47%3A43.534Z%2B0530&os_version=6.0.1"
                + "&language=en&event_token=ixseev&cpu_type=armeabi-v7a"
                + "&sent_at=2021-09-26T09%3A47%3A43.599Z%2B0530";

        String encodedData7 = URLEncoder.encode(rawData7, "UTF-8");
        OutputStream os7 = con7.getOutputStream();
        os7.write(encodedData7.getBytes());
        // end of calls

    }
}
